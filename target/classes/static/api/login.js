/**
 * 获取验证码
 * @returns {*}
 */
function getCaptcha() {
    return requests({
        url: '/captcha/acquire',
        method: 'get'
    })
}



/**
 * login
 * @param query 请求参数
 * @returns {*}
 */
function login(query) {
    return requests({
        url: '/login',
        method: 'post',
        data: query
    })
}




/**
 * 获取用户信息
 * @returns {*}
 */
function getUserInfo() {
    return requests({
        url: '/getInfo',
        method: 'get'
    })
}
